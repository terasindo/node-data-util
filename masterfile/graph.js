/*
  Typical usage:
  node masterfile/graph.js ../data/graph.in
 */
'use strict';

const yargs = require('yargs')
  .usage('Usage: $0 <output folder path>')
  .example('$0 /path/to/ouput/graph.in', 'Process the masterfile and produce the output graph')
  .demandCommand(1)
  .argv;

const xlsx = require('xlsx');
const _ = require('lodash');
const path = require('path');
const util = require('util');
const fs = require('fs-extra');
const BPromise = require('bluebird');

BPromise.promisifyAll(fs);

const argOutputFilePath = yargs._[0];
const masterfilePath = path.join(__dirname, 'transjak.xlsx');

module.exports = main;

if (module.parent === null) {
  main(argOutputFilePath);
}

function main(outputFilePath) {
  const workbook = xlsx.readFile(masterfilePath);

  const busStops = readBusStops(workbook.Sheets['bus stop']);
  const corridors = readCorridors(workbook.Sheets['corridor'], busStops);
  const edges = readEdges(workbook.Sheets['edge'], busStops, corridors);

  return writeOutput(outputFilePath, busStops, corridors, edges);
}

function readBusStops(worksheet) {
  const columns = {
    name: 'A',
    aliases: 'B',
    connectionsName: 'C',
    latitude: 'D',
    longitude: 'E',
    isTerminal: 'G',
    isCommon: 'H',
    slug: 'I'
  };

  let busStops = [];
  let row = 2;
  let id = 0;
  while (true) {
    let obj = rowToObject(worksheet, row, columns);

    if (isEmptyObject(obj)) {
      break;
    }

    var aliases = [];
    if (obj.aliases.length > 0) {
      aliases = obj.aliases.split(',');
    }

    // To be populated
    var connectionsName = [];
    if (obj.connectionsName.length > 0) {
      connectionsName = obj.connectionsName.split(',');
    }

    var busStop = {
      id: id,
      name: obj.name,
      aliases: aliases,
      connections: [],
      connectionsName: connectionsName,
      latitude: Number(obj.latitude),
      longitude: Number(obj.longitude),
      isTerminal: (obj.isTerminal == 1),
      isCommon: (obj.isCommon == 1),
      slug: obj.slug
    };

    busStops.push(busStop);
    row++;
    id++;
  }

  const busStopByName = _.keyBy(busStops, 'name');
  busStops.forEach(function (busStop) {
    busStop.connections = _.map(busStop.connectionsName, function (connectionName) {
      return busStopByName[connectionName].id;
    });
  });

  return busStops;
}

function readCorridors(worksheet, busStops) {
  const busStopByName = _.keyBy(busStops, 'name');
  const columns = {
    name: 'A',
    startBusStopName: 'B',
    endBusStopName: 'C',
    polylineFromStartCsv: 'M',
    polylineFromEndCsv: 'N',
    slug: 'O'
  };

  let corridors = [];
  let row = 2;
  let id = 0;
  while (true) {
    let obj = rowToObject(worksheet, row, columns);

    if (isEmptyObject(obj)) {
      break;
    }

    let corridor = {
      id: id,
      name: obj.name,
      fullName: util.format('%s - %s', obj.startBusStopName, obj.endBusStopName),
      polylineFromStart: parsePolylineData(obj.polylineFromStartCsv),
      polylineFromEnd: parsePolylineData(obj.polylineFromEndCsv),
      startBusStop: busStopByName[obj.startBusStopName].id,
      endBusStop: busStopByName[obj.endBusStopName].id,
      slug: obj.slug
    };

    corridors.push(corridor);
    row++;
    id++;
  }

  return corridors;
}

function readEdges(worksheet, busStops, corridors) {
  const busStopByName = _.keyBy(busStops, 'name');
  const corridorByName = _.keyBy(corridors, 'name');
  const columns = {
    fromBusStopName: 'B',
    toBusStopName: 'C',
    corridorName: 'F',
    destinationBusStopName: 'E'
  };

  let edges = [];
  let row = 2;
  let id = 0;
  while (true) {
    let obj = rowToObject(worksheet, row, columns);

    if (isEmptyObject(obj)) {
      break;
    }

    let edge = {
      fromBusStop: busStopByName[obj.fromBusStopName].id,
      toBusStop: busStopByName[obj.toBusStopName].id,
      corridor: corridorByName[obj.corridorName].id,
      destinationBusStop: busStopByName[obj.destinationBusStopName].id,
    };

    edges.push(edge);
    row++;
    id++;
  }

  return edges;
}

function writeOutput(outputFilePath, busStops, corridors, edges) {
  return fs.writeFileAsync(outputFilePath, '')
    .then(function writeBusStops() {
      let blockResult = '';

      let nAllConnection = 0;
      blockResult += busStops.length + '\n';
      busStops.forEach(function (busStop) {
        blockResult += busStop.name + '\n';
        blockResult += util.format(
          '%s %s %s %d %d %s\n',
          busStop.aliases.length,
          busStop.latitude,
          busStop.longitude,
          busStop.isTerminal ? 1 : 0,
          busStop.isCommon ? 1 : 0,
          busStop.slug
        );

        nAllConnection += busStop.connections.length;

        busStop.aliases.forEach(function (alias) {
          blockResult += alias + '\n';
        });
      });

      blockResult += nAllConnection + '\n';
      busStops.forEach(function (busStop) {
        busStop.connections.forEach(function (connection) {
          blockResult += busStop.id + ' ' + connection + '\n';
        });
      });

      return fs.appendFileAsync(outputFilePath, blockResult);
    })
    .then(function writeCorridors() {
      let blockResult = '';

      blockResult += corridors.length + '\n';
      corridors.forEach(function (corridor) {
        blockResult += corridor.name + '\n';
        blockResult += corridor.fullName + '\n';
        blockResult += util.format(
          '%s %s %s %s %s\n',
          corridor.polylineFromStart.length,
          corridor.polylineFromEnd.length,
          corridor.startBusStop,
          corridor.endBusStop,
          corridor.slug
        );

        blockResult += formatPolylineData(corridor.polylineFromStart) + '\n';
        blockResult += formatPolylineData(corridor.polylineFromEnd) + '\n';
      });

      return fs.appendFileAsync(outputFilePath, blockResult);
    })
    .then(function writeEdges() {
      let blockResult = '';

      blockResult += edges.length + '\n';
      edges.forEach(function (edge) {
        blockResult += util.format(
          '%s %s %s %s\n',
          edge.fromBusStop,
          edge.toBusStop,
          edge.corridor,
          edge.destinationBusStop
        );
      });

      return fs.appendFileAsync(outputFilePath, blockResult);
    });
}

function getCellValue(worksheet, cellAddress) {
  var cell = worksheet[cellAddress];

  if (!cell) {
    return '';
  }

  return cell.v;
}

function rowToObject(worksheet, row, columnFieldObj) {
  var obj = {};

  _.each(columnFieldObj, function (column, fieldName) {
    var cellAddress = column + row;
    obj[fieldName] = getCellValue(worksheet, cellAddress);
  });

  return obj;
}

function isEmptyObject(obj) {
  var empty = true;

  _.each(obj, function (column, fieldName) {
    if (!_.isEmpty(obj[fieldName])) {
      empty = false;
    }
  });

  return empty;
}

function parsePolylineData(str) {
  return _.chain(str.split('#'))
    .chunk(2)
    .map(function (pair) {
      return {
        latitude: pair[1],
        longitude: pair[0]
      };
    })
    .dropRight()
    .value();
}

function formatPolylineData(polyline) {
  return _.map(polyline, function (coordinate) {
    return util.format('%s %s', coordinate.latitude, coordinate.longitude);
  }).join(' ');
}