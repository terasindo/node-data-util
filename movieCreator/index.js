/*
 Typical usage
 for i in {01..31}; do node movieCreator/index.js ../data/raw/2016-06-$i/ ../data/movie/2016-06-$i.tmv; done
 */
'use strict';

const yargs = require('yargs')
  .usage('Usage: $0 <input folder path> <output file path>')
  .example('$0 /path/to/input/2016-03-21/ /path/to/ouput/2016-03-21.out', 'Process the given date data')
  .demandCommand(2)
  .argv;

var _ = require('lodash');
var fs = require('fs-extra');
var moment = require('moment');
var bpromise = require('bluebird');
var targz = require('node-targz');
var path = require('path');

bpromise.promisifyAll(fs);
bpromise.promisifyAll(targz);

module.exports = main;

var argInputFolderPath = yargs._[0];
var argOutputFilePath = yargs._[1];

if (module.parent === null) {
  main(argInputFolderPath, argOutputFilePath);
}

function main(inputFolderPath, outputFilePath) {
  var delimiter = '#';

  fs.ensureDirSync(inputFolderPath);
  return fs.readdirAsync(inputFolderPath)
    .then(function (files) {
      var compressed = {};
      var processed = 0;
      var count = files.length;

      files.forEach(function createTask(file) {
        processed++;
        console.log('Processing ' + file + ' ' + (processed/count*100).toFixed(2) + '%');
        var filePath = path.join(inputFolderPath, file);

        var content = {};
        try {
          content = fs.readJsonSync(filePath);
        } catch (err) {
          // Sometimes the result is not json, but HTML stating the API is currently broken
          content.buswaytracking = [];
        }

        content.buswaytracking.forEach(function (busPosition) {
          var id = busPosition.buscode;

          if (_.isUndefined(compressed[id])) {
            compressed[id] = {
              route: busPosition.koridor,
              positions: []
            };

            for (var i = 0; i < 24 * 60; i++) {
              compressed[id].positions.push({});
            }
          }

          var time = moment(busPosition.gpsdatetime, 'YYYY-MM-DD HH:mm:SS');
          var minutesElapsed = time.hour() * 60 + time.minute();

          compressed[id].positions[minutesElapsed] = {
            lon: busPosition.longitude,
            lat: busPosition.latitude
          };
        });
      });

      // Convert to simple .tmv (terasi movie) format
      // <bus id>#<corridor>#<lon 00:00>#<lat 00:00>#<lon 00:01>#<lat 00:01>#...
      var encode = function (compressed) {
        var buff = '';

        var busIds = _.keys(compressed);
        busIds.forEach(function (busId) {
          var info = compressed[busId];

          buff += busId + delimiter;
          buff += info.route + delimiter;
          info.positions.forEach(function (position) {
            var lon = position.lon || '';
            var lat = position.lat || '';

            buff += lon + delimiter + lat + delimiter;
          });
          buff += '\n';
        });

        return buff;
      };

      var outputFolderPath = path.join(outputFilePath, '..');
      fs.ensureDirSync(outputFolderPath);
      return fs.writeFileAsync(outputFilePath, encode(compressed));
    })
    .catch(function (err) {
      console.error(err.stack || err);
    });
}
