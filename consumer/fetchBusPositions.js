'use strict';

const yargs = require('yargs')
  .usage('Usage: $0 [options] <output folder path>')
  .example('$0 /path/to/folder --slack-webhook-url https://hooks.slack.com/services/XXX/YYY/ZZZ --slack-domain mydomain --slack-channel \'#my-channel\'')
  .options({
    'slack-webhook-url': {
      describe: 'Your slack webhook URL',
      demand: true
    },
    'slack-domain': {
      describe: 'Your slack domain',
      demand: true
    },
    'slack-channel': {
      describe: 'Your slack channel (including the \'#\' prefix)',
      demand: true
    },
    'slack-username': {
      describe: 'Slack username for this logging',
      default: 'Robot',
      demand: false
    },
    'slack-emoji': {
      describe: 'Emoji for this logging',
      default: ':robot_face:',
      demand: false
    }
  })
  .demandCommand(1)
  .argv;

const SMALL_TOLERANCE = 100;
const MAX_RETRY_COUNT = 3;
const ALMOST_1_MINUTE_SECOND = 55;
const RESPONSE = {
  SERVER_BUSY: 'Server Too Busy'
};
const PRIME_HOURS = [6, 12, 18, 21];

const _ = require('lodash');
const fs = require('fs-extra');
const curl = require('curlrequest');
const util = require('util');
const moment = require('moment');
const path = require('path');
const targz = require('node-targz');
const BPromise = require('bluebird');
const helper = require('./helper');

BPromise.promisifyAll(fs);
BPromise.promisifyAll(targz);
BPromise.promisifyAll(curl);

const argOutputFolderPath = yargs._[0];

module.exports = main;

if (module.parent === null) {
  main(argOutputFolderPath, moment(), 0)
    .then(function () {
      setTimeout(function () {
        process.exit(0);
      }, 1000);
    });
}

function main(consumedResultDir, timeCalled, retryCount) {
  const now = moment();
  const slackNotifier = helper.getSlackNotifier({
    domain: yargs['slack-domain'],
    slackWebhookUrl: yargs['slack-webhook-url'] || '',
    channel: yargs['slack-channel'],
    username: yargs['slack-username'],
    iconEmoji: yargs['slack-emoji']
  });

  return BPromise.resolve()
    .then(function () {
      var reqParams = {
        url: 'http://api.transjakarta.id/transjakarta_bus_ops_api_unified_MODIFIED.php',
        headers: {
          'Host': 'api.transjakarta.id:8088',
          'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:42.0) Gecko/20100101 Firefox/42.0',
          'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
          'Accept-Language': 'en-US,en;q=0.5',
          'Referer': 'http://smartcity.jakarta.go.id/maps/',
          'Origin': 'http://smartcity.jakarta.go.id',
          'Connection': 'keep-alive'
        }
      };

      return curl.requestAsync(reqParams);
    })
    .then(function (outputJson) {
      var success = true;

      if (outputJson.length < SMALL_TOLERANCE) {
        slackNotifier.notify(util.format('Only %s bytes retrieved from bus position API', outputJson.length));

        var shouldRetry = (retryCount < MAX_RETRY_COUNT) && (now.diff(timeCalled, 'seconds') <= ALMOST_1_MINUTE_SECOND);
        if ((outputJson.indexOf(RESPONSE.SERVER_BUSY) !== -1) && shouldRetry) {
          // Retry
          return main(timeCalled, retryCount + 1);
        } else {
          success = false;
        }
      }

      var folder = path.join(consumedResultDir, now.format('YYYY-MM-DD'));
      var file = path.join(folder, now.format('x') + '.json');

      fs.ensureDirSync(folder);

      return fs.outputFileAsync(file, outputJson)
        .then(function () {
          if (success && isPrimeTime(now)) {
            slackNotifier.notify(util.format('Just want to tell you that API consuming is still working fine (at least just now)'));
          }
        });
    })
    .catch(function (err) {
      slackNotifier.notify(err.stack || err);
    });
}

function isPrimeTime(momentTime) {
  return _.some(PRIME_HOURS, function (primeHour) {
    var h = momentTime.hour();
    var m = momentTime.minute();
    var s = momentTime.second();

    var thSecond = h*60*60 + m*60 + s;
    var primeThSecond = primeHour*60*60;
    var window = 59;

    return (primeThSecond <= thSecond) && (thSecond <= primeThSecond + window);
  });
}
