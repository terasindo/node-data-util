'use strict';

const yargs = require('yargs')
  .usage('Usage: $0 [options] <output folder path>')
  .example('$0 /path/to/folder --slack-webhook-url https://hooks.slack.com/services/XXX/YYY/ZZZ --slack-domain mydomain --slack-channel \'#my-channel\'')
  .options({
    'slack-webhook-url': {
      describe: 'Your slack webhook URL',
      demand: true
    },
    'slack-domain': {
      describe: 'Your slack domain',
      demand: true
    },
    'slack-channel': {
      describe: 'Your slack channel (including the \'#\' prefix)',
      demand: true
    },
    'slack-username': {
      describe: 'Slack username for this logging',
      default: 'Robot',
      demand: false
    },
    'slack-emoji': {
      describe: 'Emoji for this logging',
      default: ':robot_face:',
      demand: false
    }
  })
  .demandCommand(1)
  .argv;

const _ = require('lodash');
const fs = require('fs-extra');
const curl = require('curlrequest');
const util = require('util');
const moment = require('moment');
const path = require('path');
const targz = require('node-targz');
const BPromise = require('bluebird');

BPromise.promisifyAll(fs);
BPromise.promisifyAll(targz);
BPromise.promisifyAll(curl);

const helper = require('./helper');

const argOutputFolderPath = yargs._[0];

module.exports = main;

if (module.parent === null) {
  main(argOutputFolderPath)
    .then(function () {
      setTimeout(function () {
        process.exit(0);
      }, 1000);
    });
}

function main(consumedResultDir) {
  const yesterday = moment().subtract(1, 'days').format('YYYY-MM-DD');
  const slackNotifier = helper.getSlackNotifier({
    domain: yargs['slack-domain'],
    slackWebhookUrl: yargs['slack-webhook-url'] || '',
    channel: yargs['slack-channel'],
    username: yargs['slack-username'],
    iconEmoji: yargs['slack-emoji']
  });

  const sourceDir = path.join(consumedResultDir, yesterday);
  const compressFileName = util.format('%s.tar.gz', yesterday);
  const compressDestinationFilePath = path.join(consumedResultDir, compressFileName);

  return BPromise.resolve()
    .then(function () {
      return targz.compressAsync({
        source: sourceDir,
        destination: compressDestinationFilePath,
      });
    })
    .then(function removeFolder() {
      var size = _.get(fs.statSync(compressDestinationFilePath), 'size');
      slackNotifier.notify(util.format(
        'You got %d MB worth of bus positions yesterday',
        _.round(size / Math.pow(10, 6), 2)
      ));

      return fs.removeAsync(path.join(consumedResultDir, yesterday));
    })
    .catch(function (err) {
      slackNotifier.notify(err.stack || err);
    });
}
