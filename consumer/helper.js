'use strict';

const _ = require('lodash');
const request = require('request');

module.exports.getSlackNotifier = function (options = {}) {
  return {
    notify: function (message, callback = _.noop) {
      const payload = {
        channel: options.channel,
        username: options.username,
        text: '```' + message + '```',
        icon_emoji: options.iconEmoji
      };

      return request.post({
        url: options.slackWebhookUrl,
        form: {payload: JSON.stringify(payload)}
      }, callback);
    }
  };
};