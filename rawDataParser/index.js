/*
 Typical usage
 for i in {01..31}; do node rawDataParser/index.js ../data/raw/2016-06-$i/ ../data/positionLog/2016-06-$i.plog; done
 */
'use strict';

const yargs = require('yargs')
  .usage('Usage: $0 <input folder path> <output file path>')
  .example('$0 /path/to/input/2016-03-21/ /path/to/ouput/2016-03-21.out', 'Process the given date data')
  .demandCommand(2)
  .argv;

const _ = require('lodash');
const path = require('path');
const util = require('util');
const fs = require('fs-extra');
const moment = require('moment');
const BPromise = require('bluebird');

BPromise.promisifyAll(fs);

const argInputFolderPath = yargs._[0];
const argOutputFilePath = yargs._[1];

module.exports = main;

if (module.parent === null) {
  main(argInputFolderPath, argOutputFilePath);
}

function main(inputFolderPath, outputFilePath) {
  console.log('Loading ' + inputFolderPath);
  return fs.readdirAsync(inputFolderPath)
    .then(function (files) {
      let dateString = inputFolderPath.split(' ')[0];
      let date = moment(dateString, 'YYYY-MM-DD').hour(0).minute(0).second(0).millisecond(0);

      return fs.writeFileAsync(outputFilePath, '')
        .then(function () {
          let progressCount = 0;
          let allCount = files.length;
          return BPromise.each(files, function (file) {
            progressCount += 1;
            let filePath = path.join(inputFolderPath, file);
            console.log('Loading ' + filePath + ' ' + (progressCount/allCount*100).toFixed(2) + '%');
            return appendOutput(outputFilePath, filePath);
          });
        });
    })
    .catch(function (err) {
      if (err.code === 'ENOENT') {
        return fs.writeFileAsync(outputFilePath, '');
      } else {
        console.error(err);
      }
    });
}

function appendOutput(outputFilePath, filePath) {
  return fs.readJsonAsync(filePath)
    .then(function (content) {
      let blockResult = '';
      content.buswaytracking.forEach(function (busPosition) {
        let id = busPosition.buscode;

        let gpsTime = moment(busPosition.gpsdatetime, 'YYYY-MM-DD HH:mm:SS');

        blockResult += util.format(
          '%s\n%s %s %s %s\n',
          id,
          gpsTime.format('X'),
          busPosition.latitude,
          busPosition.longitude,
          busPosition.koridor || '?' // If it is empty, replace with '?'
        );
      });

      return fs.appendFileAsync(outputFilePath, blockResult);
    })
    .catch(function () {
      // Sometimes the result is not json, but HTML stating the API is currently broken
    });
}
